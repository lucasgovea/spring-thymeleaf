package com.lgovea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GigaNinjaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GigaNinjaApplication.class, args);
	}
}
