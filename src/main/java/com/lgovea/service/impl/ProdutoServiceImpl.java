package com.lgovea.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lgovea.domain.Produto;
import com.lgovea.repository.ProdutoRepository;
import com.lgovea.service.IProdutoService;

/**
 * @author Lucas Govea
 */

@Service
public class ProdutoServiceImpl implements IProdutoService{
	
	@Autowired
	private ProdutoRepository produtoRepo;
	
	@Override
	public Page<Produto> obterTodos(Pageable pageable) {
		return produtoRepo.findAll(pageable);
	}

	@Override
	public Page<Produto> obterTodosPorNomeContem(String termo, Pageable pageable) {
		return produtoRepo.findByNomeContainingIgnoreCase(termo, pageable);
	}

	@Override
	public Produto salvar(Produto produto) {
		return produtoRepo.save(produto);
	}

	@Override
	public void excluir(Produto produto) {
		produtoRepo.delete(produto);
	}

	@Override
	public List<Produto> obterTodos() {
		return produtoRepo.findAll();
	}

}
