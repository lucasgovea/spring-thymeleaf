package com.lgovea.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lgovea.domain.Fornecedor;
import com.lgovea.repository.FornecedorRepository;
import com.lgovea.service.IFornecedorService;

/**
 * @author Lucas Govea
 */

@Service
public class FornecedorServiceImpl implements IFornecedorService{

	@Autowired
	private FornecedorRepository fornecedorRepo;
	
	@Override
	public Page<Fornecedor> obterTodos(Pageable pageable) {
		return fornecedorRepo.findAll(pageable);
	}
	
	@Override
	public List<Fornecedor> obterTodos() {
		return fornecedorRepo.findAll();
	}

	@Override
	public Page<Fornecedor> obterTodosPorNomeContem(String termo, Pageable pageable) {
		return fornecedorRepo.findByNomeContainingIgnoreCase(termo, pageable);
	}

	@Override
	public Fornecedor salvar(Fornecedor fornecedor) {
		return fornecedorRepo.save(fornecedor);
		
	}

	@Override
	public void excluir(Fornecedor fornecedor) {
		fornecedorRepo.delete(fornecedor);
	}

}
