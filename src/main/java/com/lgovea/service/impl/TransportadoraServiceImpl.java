package com.lgovea.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lgovea.domain.Transportadora;
import com.lgovea.repository.TransportadoraRepository;
import com.lgovea.service.ITransportadoraService;

/**
 * @author Lucas Govea
 */

@Service
public class TransportadoraServiceImpl implements ITransportadoraService{

	@Autowired
	private TransportadoraRepository transportadoraRepo;
	
	@Override
	public Page<Transportadora> obterTodos(Pageable pageable) {
		return transportadoraRepo.findAll(pageable);
	}

	@Override
	public Page<Transportadora> obterTodosPorNomeContem(String termo, Pageable pageable) {
		return transportadoraRepo.findByNomeContainingIgnoreCase(termo, pageable);
	}

	@Override
	public Transportadora salvar(Transportadora transportadora) {
		return transportadoraRepo.save(transportadora);
	}
	
	@Override
	public void excluir(Transportadora transportadora) {
		transportadoraRepo.delete(transportadora);
	}

	@Override
	public List<Transportadora> obterTodos() {
		return transportadoraRepo.findAll();
	}
}
