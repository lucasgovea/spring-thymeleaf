package com.lgovea.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lgovea.domain.Item;
import com.lgovea.domain.Pedido;
import com.lgovea.repository.PedidoRepository;
import com.lgovea.service.IPedidoService;

/**
 * @author Lucas Govea
 */

@Service
public class PedidoServiceImpl implements IPedidoService{

	@Autowired
	private PedidoRepository pedidoRepo;
	
	@Override
	public Page<Pedido> obterTodos(Pageable pageable) {
		return pedidoRepo.findAll(pageable);
	}

	@Override
	public Page<Pedido> obterTodosPorNotaFiscalContem(String termo, Pageable pageable) {
		return pedidoRepo.findByNotaFiscalContainingIgnoreCase(termo, pageable);
	}

	@Override
	public Pedido salvar(Pedido pedido) {
		for (Item item : pedido.getItens()) {
			item.setPedido(pedido);
		}
		pedido.setDataHora(Calendar.getInstance());
		return pedidoRepo.save(pedido);
	}

	@Override
	public void excluir(Pedido pedido) {
		pedidoRepo.delete(pedido);
	}

	@Override
	public Pedido obterPorId(int idPedido) {
		return pedidoRepo.findOne(idPedido);
	}

}
