package com.lgovea.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lgovea.domain.Pedido;

/**
 * @author Lucas Govea
 */

public interface IPedidoService{

	public Page<Pedido> obterTodos(Pageable pageable);

	public Page<Pedido> obterTodosPorNotaFiscalContem(String termo, Pageable pageable);

	public Pedido salvar(Pedido pedido);

	public void excluir(Pedido pedido);

	public Pedido obterPorId(int idPedido);
}
