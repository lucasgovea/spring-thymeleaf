package com.lgovea.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lgovea.domain.Transportadora;

/**
 * @author Lucas Govea
 */

public interface ITransportadoraService{

	public Page<Transportadora> obterTodos(Pageable pageable);

	public Page<Transportadora> obterTodosPorNomeContem(String termo, Pageable pageable);

	public Transportadora salvar(Transportadora transportadora);

	public void excluir(Transportadora transportadora);

	public List<Transportadora> obterTodos();
	
}
