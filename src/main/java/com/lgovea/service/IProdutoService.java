package com.lgovea.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lgovea.domain.Produto;

/**
 * @author Lucas Govea
 */

public interface IProdutoService{

	public Page<Produto> obterTodos(Pageable pageable);

	public Page<Produto> obterTodosPorNomeContem(String termo, Pageable pageRequest);
	
	public Produto salvar(Produto produto);

	public void excluir(Produto produto);

	public List<Produto> obterTodos();
	
}
