package com.lgovea.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lgovea.domain.Fornecedor;

/**
 * @author Lucas Govea
 */

public interface IFornecedorService{

	public Page<Fornecedor> obterTodos(Pageable pageable);
	
	public List<Fornecedor> obterTodos();

	public Page<Fornecedor> obterTodosPorNomeContem(String termo, Pageable pageable);

	public Fornecedor salvar(Fornecedor fornecedor);

	public void excluir(Fornecedor fornecedor);
}
