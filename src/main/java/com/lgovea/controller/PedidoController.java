package com.lgovea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lgovea.domain.Pedido;
import com.lgovea.pagination.Pager;
import com.lgovea.service.IPedidoService;
import com.lgovea.service.IProdutoService;
import com.lgovea.service.ITransportadoraService;

/**
 * @author Lucas Govea
 */

@Controller
@RequestMapping("/pedidos")
public class PedidoController {
	
	private static final int BUTTONS_TO_SHOW = 10;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;
	
	@Autowired
	private IPedidoService pedidoService;
	
	@Autowired
	private IProdutoService produtoService;

	@Autowired
	private ITransportadoraService transportadoraService;

	@RequestMapping(method=RequestMethod.GET)	
    public ModelAndView listarPedidos(Integer pageSize,@RequestParam(value = "page", required = false) Integer page) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		Page<Pedido> pedidos = pedidoService.obterTodos(new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(pedidos.getTotalPages(), pedidos.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/pedido/pedidos");
		mav.addObject("pedidos", pedidos);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/buscar")	
	public ModelAndView buscarPedidos(Integer pageSize, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "termoPesquisa", required = false) String termo) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		termo = termo == null ? "%" : termo;
		
		Page<Pedido> pedidos = pedidoService.obterTodosPorNotaFiscalContem(termo, new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(pedidos.getTotalPages(), pedidos.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/pedido/pedidos");
		mav.addObject("pedidos", pedidos);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/visualizar/{id}")	
	public ModelAndView visualizarPedido(@PathVariable("id") Pedido pedido) {
		ModelAndView mav = new ModelAndView("/pedido/dadosPedido");
		mav.addObject("ehVisualizar", true);
		mav.addObject("pedido", pedido);
		mav.addObject("transportadoras", transportadoraService.obterTodos());
		mav.addObject("produtos", produtoService.obterTodos());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/novo")	
	public ModelAndView novoPedido() {
		
		ModelAndView mav = new ModelAndView("/pedido/dadosPedido");
		mav.addObject("pedido", new Pedido());
		mav.addObject("transportadoras", transportadoraService.obterTodos());
		mav.addObject("produtos", produtoService.obterTodos());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value= "/salvar")	
	public String salvarPedido(Pedido pedido) {
		pedidoService.salvar(pedido);
		return "redirect:/pedidos";
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value= "/excluir/{id}")	
	public @ResponseBody HttpStatus excluirPedido(@PathVariable("id") int idPedido) {
		Pedido pedido = pedidoService.obterPorId(idPedido);
		pedidoService.excluir(pedido);
		return HttpStatus.OK;
	}
	
}
