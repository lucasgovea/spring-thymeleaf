package com.lgovea.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Lucas Govea
 */

@Controller
@RequestMapping("/")
public class MainController {

	@RequestMapping(method=RequestMethod.GET)	
    public String index() {
		return "/index";
	}
	
}
