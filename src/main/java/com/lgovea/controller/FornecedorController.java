package com.lgovea.controller;

/**
 * @author Lucas Govea
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lgovea.domain.Fornecedor;
import com.lgovea.pagination.Pager;
import com.lgovea.service.IFornecedorService;

@Controller
@RequestMapping("/fornecedores")
public class FornecedorController {

	private static final int BUTTONS_TO_SHOW = 10;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;
	
	@Autowired
	private IFornecedorService fornecedorService;

	@RequestMapping(method=RequestMethod.GET)	
    public ModelAndView listarFornecedores(Integer pageSize,@RequestParam(value = "page", required = false) Integer page) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		Page<Fornecedor> fornecedores = fornecedorService.obterTodos(new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(fornecedores.getTotalPages(), fornecedores.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/fornecedor/fornecedores");
		mav.addObject("fornecedores", fornecedores);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/buscar")	
	public ModelAndView buscarFornecedores(Integer pageSize, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "termoPesquisa", required = false) String termo) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		termo = termo == null ? "%" : termo;
		
		Page<Fornecedor> fornecedores = fornecedorService.obterTodosPorNomeContem(termo, new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(fornecedores.getTotalPages(), fornecedores.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/fornecedor/fornecedores");
		mav.addObject("fornecedores", fornecedores);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/editar/{id}")	
	public ModelAndView editarFornecedores(@PathVariable("id") Fornecedor fornecedor) {
		
		ModelAndView mav = new ModelAndView("/fornecedor/dadosFornecedor");
		mav.addObject("fornecedor", fornecedor);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/novo")	
	public ModelAndView novoProduto() {
		
		ModelAndView mav = new ModelAndView("/fornecedor/dadosFornecedor");
		mav.addObject("fornecedor", new Fornecedor());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value= "/salvar")	
	public String salvarFornecedor(Fornecedor fornecedor) {
		fornecedorService.salvar(fornecedor);
		return "redirect:/fornecedores";
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value= "/excluir/{id}")	
	public @ResponseBody HttpStatus excluirProduto(@PathVariable("id") Fornecedor fornecedor) {
		fornecedorService.excluir(fornecedor);
		return HttpStatus.OK;
	}
	
}
