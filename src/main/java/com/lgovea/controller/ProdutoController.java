package com.lgovea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lgovea.domain.Produto;
import com.lgovea.pagination.Pager;
import com.lgovea.service.IFornecedorService;
import com.lgovea.service.IProdutoService;

/**
 * @author Lucas Govea
 */

@Controller
@RequestMapping("/produtos")
public class ProdutoController {
	
	private static final int BUTTONS_TO_SHOW = 10;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;
	
	@Autowired
	private IProdutoService produtoService;
	
	@Autowired
	private IFornecedorService fornecedorService;

	@RequestMapping(method=RequestMethod.GET)	
    public ModelAndView listarProdutos(Integer pageSize,@RequestParam(value = "page", required = false) Integer page) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		Page<Produto> produtos = produtoService.obterTodos(new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(produtos.getTotalPages(), produtos.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/produto/produtos");
		mav.addObject("produtos", produtos);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/buscar")	
	public ModelAndView buscarProdutos(Integer pageSize, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "termoPesquisa", required = false) String termo) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		termo = termo == null ? "%" : termo;
		
		Page<Produto> produtos = produtoService.obterTodosPorNomeContem(termo, new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(produtos.getTotalPages(), produtos.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/produto/produtos");
		mav.addObject("produtos", produtos);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/editar/{id}")	
	public ModelAndView editarProdutos(@PathVariable("id") Produto produto) {
		
		ModelAndView mav = new ModelAndView("/produto/dadosProduto");
		mav.addObject("produto", produto);
		mav.addObject("fornecedores", fornecedorService.obterTodos());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/novo")	
	public ModelAndView novoProduto() {
		
		ModelAndView mav = new ModelAndView("/produto/dadosProduto");
		mav.addObject("produto", new Produto());
		mav.addObject("fornecedores", fornecedorService.obterTodos());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value= "/salvar")	
	public String salvarProduto(Produto produto) {
		produtoService.salvar(produto);
		return "redirect:/produtos";
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value= "/excluir/{id}")	
	public @ResponseBody HttpStatus excluirProduto(@PathVariable("id") Produto produto) {
		produtoService.excluir(produto);
		return HttpStatus.OK;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/obter-todos")	
	public @ResponseBody List<Produto> obterTodosProduto() {
		
		return produtoService.obterTodos();
	}
	
}
