package com.lgovea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lgovea.domain.Transportadora;
import com.lgovea.pagination.Pager;
import com.lgovea.service.ITransportadoraService;

/**
 * @author Lucas Govea
 */


@Controller
@RequestMapping("/transportadoras")
public class TransportadoraController {
	
	private static final int BUTTONS_TO_SHOW = 10;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 10;

	@Autowired
	private ITransportadoraService transportadoraService;
	
	
	@RequestMapping(method=RequestMethod.GET)	
    public ModelAndView listarTransportadoras(Integer pageSize,@RequestParam(value = "page", required = false) Integer page) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		Page<Transportadora> transportadoras = transportadoraService.obterTodos(new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(transportadoras.getTotalPages(), transportadoras.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/transportadora/transportadoras");
		mav.addObject("transportadoras", transportadoras);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/buscar")	
	public ModelAndView buscarTransportadoras(Integer pageSize, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "termoPesquisa", required = false) String termo) {
		
		int evalPageSize = pageSize == null ? INITIAL_PAGE_SIZE : pageSize;
		int evalPage = (page == null || page < 1) ? INITIAL_PAGE : page - 1;
		
		termo = termo == null ? "%" : termo;
		
		Page<Transportadora> transportadoras = transportadoraService.obterTodosPorNomeContem(termo, new PageRequest(evalPage, evalPageSize));
		Pager pager = new Pager(transportadoras.getTotalPages(), transportadoras.getNumber(), BUTTONS_TO_SHOW);
		
		ModelAndView mav = new ModelAndView("/transportadora/transportadoras");
		mav.addObject("transportadoras", transportadoras);
		mav.addObject("pager", pager);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/editar/{id}")	
	public ModelAndView editarTransportadoras(@PathVariable("id") Transportadora transportadora) {
		
		ModelAndView mav = new ModelAndView("/transportadora/dadosTransportadora");
		mav.addObject("transportadora", transportadora);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/novo")	
	public ModelAndView novoProduto() {
		
		ModelAndView mav = new ModelAndView("/transportadora/dadosTransportadora");
		mav.addObject("transportadora", new Transportadora());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value= "/salvar")	
	public String salvarProduto(Transportadora transportadora) {
		transportadoraService.salvar(transportadora);
		return "redirect:/transportadoras";
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value= "/excluir/{id}")	
	public @ResponseBody HttpStatus excluirProduto(@PathVariable("id") Transportadora transportadora) {
		transportadoraService.excluir(transportadora);
		return HttpStatus.OK;
	}
	
}
