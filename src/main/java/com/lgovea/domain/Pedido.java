package com.lgovea.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

/**
 * @author Lucas Govea
 */

@Entity
@Table(name="PEDIDO")
public class Pedido {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm:ss")
	@Column(name="DATAHORA")
	private Calendar dataHora;

	@Column(name="NOTAFISCAL")
	private String notaFiscal;

	@NumberFormat(pattern = "#,##0.00")
	@Column(name="VALORFRETE")
	private float valorFrete = 0.00f;

	@NumberFormat(pattern = "#,##0.00")
	@Column(name="DESCONTO")
	private float desconto = 0.00f;

	@NumberFormat(pattern = "#,##0.00")
	@Column(name="VALORTOTAL")
	private float valorTotal = 0.00f;

	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="pedido", orphanRemoval = true)
	private List<Item> itens = new ArrayList<Item>();

	@ManyToOne
    @JoinColumn(name = "IDTRANSPORTADORA")
	private Transportadora transportadora;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Calendar getDataHora() {
		return dataHora;
	}

	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}

	public String getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(String notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public float getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(float valorFrete) {
		this.valorFrete = valorFrete;
	}

	public float getDesconto() {
		return desconto;
	}

	public void setDesconto(float desconto) {
		this.desconto = desconto;
	}

	public float getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(float valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Transportadora getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(Transportadora transportadora) {
		this.transportadora = transportadora;
	}

}
