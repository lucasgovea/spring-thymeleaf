package com.lgovea.domain;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Lucas Govea
 */

@Entity
@Table(name="FORNECEDOR")
@JsonIgnoreProperties({ "produtos", "emails", "telefones"})
public class Fornecedor {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="NOME")
	private String nome;

	@Column(name="DESCRICAO")
	private String descricao;

	@Column(name="CIDADE")
	private String cidade;

	@Column(name="ENDERECO")
	private String endereco;

	@Column(name="BAIRRO")
	private String bairro;

	@Column(name="NUMERO")
	private int numero;

	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="fornecedor", orphanRemoval = true)
	private List<Email> emails = new ArrayList<Email>();

	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="fornecedor", orphanRemoval = true)
	private List<Produto> produtos = new ArrayList<Produto>();

	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="fornecedor", orphanRemoval = true)
	private List<Telefone> telefones = new ArrayList<Telefone>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

}
