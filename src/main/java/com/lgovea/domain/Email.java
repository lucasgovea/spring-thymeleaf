package com.lgovea.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lucas Govea
 */

@Entity
@Table(name="EMAIL")
public class Email {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@JoinColumn(name="EMAIL")
	private String email;

	@JoinColumn(name="REFERENCIA")
	private String referencia;
	
	@ManyToOne
	@JoinColumn(name="IDFORNECEDOR")
	private Fornecedor fornecedor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

}
