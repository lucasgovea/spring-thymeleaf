package com.lgovea.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lgovea.domain.Produto;

/**
 * @author Lucas Govea
 */

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

	public Page<Produto> findByNomeContainingIgnoreCase(String termo, Pageable pageable);
	
}
