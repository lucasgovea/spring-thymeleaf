package com.lgovea.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lgovea.domain.Fornecedor;

/**
 * @author Lucas Govea
 */

@Repository
public interface FornecedorRepository extends JpaRepository<Fornecedor, Integer> {

	Page<Fornecedor> findByNomeContainingIgnoreCase(String termo, Pageable pageable);

}
