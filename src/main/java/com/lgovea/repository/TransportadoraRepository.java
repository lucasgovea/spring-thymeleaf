package com.lgovea.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.lgovea.domain.Transportadora;

/**
 * @author Lucas Govea
 */

public interface TransportadoraRepository extends JpaRepository<Transportadora, Integer> {

	public Page<Transportadora> findByNomeContainingIgnoreCase(String termo, Pageable pageable);
	
}
