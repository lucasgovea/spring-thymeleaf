package com.lgovea.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lgovea.domain.Pedido;

/**
 * @author Lucas Govea
 */

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Integer> {

	public Page<Pedido> findByNotaFiscalContainingIgnoreCase(String termo, Pageable pageable);
	
}
