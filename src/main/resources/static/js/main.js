$(document).ready(function() {

	$('#js-excluir-modal').on('show.bs.modal', function(e){
		var button = $(e.relatedTarget);
		var id = button.data('id');
		var link = button.data('link');
		$('#simButton').on('click', function(e){
			$.ajax({
				url: "/"+link+"/excluir/"+id,
				method: "DELETE",
				success: function(result){
					$('#js-excluir-modal').modal('hide');
					location.reload();
				},
				error: function(result){
					console.log(result);
				}
			});
		});
	});


	$('#dadosFornecedorForm').on('submit',function(){
		if($('#nome').val().trim() == '' ||
		   $('#descricao').val().trim() == '' || 
		   $('#cidade').val().trim() == '' || 
		   $('#endereco').val().trim() == '' || 
		   $('#bairro').val().trim() == '' || 
		   $('#numero').val().trim() == ''){
				$('.alert').removeClass('hide');
				return false;
		}
		return true;
	});


	$('#dadosTransportadoraForm').on('submit',function(){
		if($('#nome').val().trim() == ''){
			$('.alert').removeClass('hide');
			return false;
		}
		return true;
	});


	$('#dadosProdutoForm').on('submit',function(){
		if($('#nome').val().trim() == '' || $('#descricao').val().trim() == '' || $('#fornecedor :selected').val().trim() == ''){
			$('.alert').removeClass('hide');
			return false;
		}
		return true;
	});
	
	
	/** adiciona item ao pedido e calcula o valor**/
	var counter = 0;
    $("#addrow").on("click", function (e) {
        e.preventDefault();
        
        $.ajax({
			url: "/produtos/obter-todos",
			type: "GET",
			success: function(data){
				$("div.line").children("div.linesnew").each(function(){
		            counter++;
		        });

		        var newRow ="<div id='linha"+counter+"' class='linesnew'>";
		         	newRow +="	<div class='row'>";
		         	newRow +="		<div class='form-group col-md-2'>";
		         	newRow +="			<label class='control-label'>Quantidade:</label>";
		         	newRow +="			<input type='text' name='itens["+counter+"].quantidade' class='form-control quantidade quantidade-"+counter+"'/>";
		         	newRow +="		</div>";
		         	newRow +="		<div class='form-group col-md-6'>";
		         	newRow +="	  		<label class='control-label'>Produto:</label>";
		         	newRow +="			<select class='form-control produto referencia-"+counter+"-produto' name='itens["+counter+"].produto'>";
		         	newRow +="					<option value=''>Selecione...</option>";
	         		for (i=0; i<data.length;i++){
	         			newRow +="				<option value="+data[i].id+">"+data[i].nome+"</option>";
	         		}
		         	newRow +="			</select>";
		         	newRow +="		</div>";
		         	newRow +="  	<div class='form-group col-md-3'>";
		         	newRow +="			<label class='control-label'>Valor(R$)</label>";
		         	newRow +="			<input type='text' class='form-control valor valor-"+counter+"' name='itens["+counter+"].valor'/>";
		         	newRow +="		</div>";
		         	newRow +="		<div class='form-group col-md-1 text-right'>";
		         	newRow +="			<label class='control-label'>&nbsp;</label><br/>";
		         	newRow +="			<button type='button' class='btn btn-danger botao-excluir' data-numberline='"+counter+"'><span class='glyphicon glyphicon-trash'></span></button>";
		         	newRow +="		</div>";
		         	newRow +="	</div>";
		         	newRow +="</div>";

		        $("div.line").append(newRow);
		        $('.valor').mask('#0,00',{reverse:true,selectOnFocus: true});
		        $('.quantidade').mask('#0,00',{reverse:true,selectOnFocus: true});
		        gerenciarPedido(counter)
			},
			error: function(data){
				console.log(data);
			}
		});
        
    });
    
    $('.valor-total').mask('###0,00',{reverse:true,selectOnFocus: true});
    $('.valor-frete').mask('###0,00',{reverse:true,selectOnFocus: true});
    $('.desconto').mask('##0,00',{reverse:true,selectOnFocus: true});
    
    $(document).on('click','.botao-excluir', function (){
    	var linha = $(this).data('numberline');
    	$("#linha"+linha).remove();
    	calcularPedido();
    });
    
    function gerenciarPedido(counter){
		$('.quantidade-'+counter).on('keyup', function(){
				calcularPedido();
		});
		
		$('.valor-'+counter).on('keyup', function(){
			calcularPedido();
		});
		
		$('.desconto').on('keyup', function(){
				calcularPedido();
		});
		
		$('.valor-frete').on('keyup', function(){
				calcularPedido();
		});
		
    }
    
    function calcularPedido(){
    	var total=0;
    	$('.quantidade').each(function(){
    		var valorUnitario = parseFloat($(this).parent().parent().find("input.valor").val().replace(',','.'));
    		var quantidade = Number($(this).val());
    		if(!isNaN(valorUnitario) && !isNaN(quantidade)){
    			total += valorUnitario*quantidade;
    		}
    	})
    	var desconto = parseFloat($('.desconto').val());
    	if(!isNaN(desconto)){
    		total -= total*(desconto/100);
		}
    	total += Number($('.valor-frete').val().replace(',','.'));
    	$('.valor-total').val(total.toFixed(2).replace('.',','));
    }
    
    
    $('#dadosPedidoForm').on('submit', function(){
    	
    	if($('.transportadora :selected').val() == ''){
    		$('.alert').removeClass('hide');
    		return false;
    	}
    	if($('.nota-fiscal').val().trim() == ''){
    		$('.alert').removeClass('hide');
    		return false;
    	}
    	if($('.valor-frete').val().trim() == ''){
    		$('.alert').removeClass('hide');
    		return false;
    	}
    	if($('.desconto').val().trim() == ''){
    		$('.alert').removeClass('hide');
    		return false;
    	}
    	
    	if($('.linesnew').length == 0){
    		$('.alert').removeClass('hide');
    		return false;
    	}
    	var retorno = true;
    	$('.linesnew').each(function(){
    		if($(this).find("input.quantidade").val().trim() == '' ||
    				$(this).find("input.quantidade").val() == 0 ||
    				$(this).find("input.valor").val().trim() == '' ||
    				$(this).find("input.valor").val() == 0 ||
    				$(this).find("select.produto").val().trim() == '' ||
    				$(this).find("select.produto").val() == 0){
    			$('.alert').removeClass('hide');
    			retorno = false;
    		}
    	});
    	return retorno;
    	
    	
    });
    
    

});