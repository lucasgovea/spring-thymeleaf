-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE DATABASE 'example' DEFAULT CHARACTER SET utf8 ;
USE 'example' ;

-- -----------------------------------------------------
-- Table 'fornecedor'
-- -----------------------------------------------------
CREATE TABLE 'fornecedor' (
  'id' INT NULL AUTO_INCREMENT,
  'nome' VARCHAR(255) NULL,
  'descricao' VARCHAR(255) NULL,
  'cidade' VARCHAR(255) NULL,
  'endereco' VARCHAR(255) NULL,
  'bairro' VARCHAR(255) NULL,
  'numero' INT NULL,
  PRIMARY KEY ('id'));


-- -----------------------------------------------------
-- Table 'email'
-- -----------------------------------------------------
CREATE TABLE 'email' (
  'id' INT NULL AUTO_INCREMENT,
  'email' VARCHAR(255) NULL,
  'referencia' VARCHAR(255) NULL,
  'idfornecedor' INT NULL,
  PRIMARY KEY ('id'),
  INDEX 'emailFornecedor_idx' ('idfornecedor' ASC),
  CONSTRAINT 'emailFornecedor'
    FOREIGN KEY ('idfornecedor')
    REFERENCES 'fornecedor' ('id')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table 'produto'
-- -----------------------------------------------------
CREATE TABLE 'produto' (
  'id' INT NULL,
  'nome' VARCHAR(255) NULL,
  'descricao' VARCHAR(255) NULL,
  'idfornecedor' INT NULL,
  PRIMARY KEY ('id'),
  INDEX 'produtoFornecedor_idx' ('idfornecedor' ASC),
  CONSTRAINT 'produtoFornecedor'
    FOREIGN KEY ('idfornecedor')
    REFERENCES 'fornecedor' ('id')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table 'transportadora'
-- -----------------------------------------------------
CREATE TABLE 'transportadora' (
  'id' INT NULL,
  'nome' VARCHAR(255) NULL,
  PRIMARY KEY ('id'));


-- -----------------------------------------------------
-- Table 'pedido'
-- -----------------------------------------------------
CREATE TABLE 'pedido' (
  'id' INT NULL AUTO_INCREMENT,
  'datahora' DATETIME NULL,
  'notafiscal' VARCHAR(255) NULL,
  'valorFrete' FLOAT NULL,
  'desconto' FLOAT NULL,
  'valorTotal' FLOAT NULL,
  'idtransportadora' INT NULL,
  PRIMARY KEY ('id'),
  INDEX 'pedidoTransportadora_idx' ('idtransportadora' ASC),
  CONSTRAINT 'pedidoTransportadora'
    FOREIGN KEY ('idtransportadora')
    REFERENCES 'transportadora' ('id')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table 'item'
-- -----------------------------------------------------
CREATE TABLE 'item' (
  'id' INT NULL AUTO_INCREMENT,
  'quantidade' FLOAT NULL,
  'valor' FLOAT NULL,
  'idproduto' INT NULL,
  'idpedido' INT NULL,
  PRIMARY KEY ('id'),
  INDEX 'itemPrduto_idx' ('idpedido' ASC),
  CONSTRAINT 'itemPrduto'
    FOREIGN KEY ('idpedido')
    REFERENCES 'produto' ('id')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT 'itemPedido'
    FOREIGN KEY ('idpedido')
    REFERENCES 'pedido' ('id')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table 'telefone'
-- -----------------------------------------------------
CREATE TABLE 'telefone' (
  'id' INT NULL AUTO_INCREMENT,
  'ddd' VARCHAR(255) NULL,
  'numero' VARCHAR(255) NULL,
  'referencia' VARCHAR(255) NULL,
  'idfornecedor' INT NULL,
  PRIMARY KEY ('id'),
  INDEX 'tefefoneFornecedor_idx' ('idfornecedor' ASC),
  CONSTRAINT 'tefefoneFornecedor'
    FOREIGN KEY ('idfornecedor')
    REFERENCES 'fornecedor' ('id')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);