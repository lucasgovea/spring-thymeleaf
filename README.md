# Implementação Projeto #

### Ferramentas utilizadas ###
* Java 8
* Spring Boot
* Thymeleaf
* H2
* Maven
* Bootstrap

### Como rodar ###

1. Instalar o STS (Spring Tool Suite)
2. Caso o projeto esteja com erro, rodar o Maven
3. Rodar projeto como "Spring Boot App"
4. Acesse pelo navegador usando "http://localhost:8080/"